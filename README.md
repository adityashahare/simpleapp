# SimpleApp

This repository contains a simple standalone application and a CI/CD pipeline using GitLab and Jenkins.

## Prerequisites

- Jenkins installed and configured
- Python installed on the Jenkins server
- GitLab repository created

## How to Set Up and Run the CI/CD Pipeline

1. Clone this repository:

   ```bash
   git clone https://gitlab.com/adityashahare/SimpleApp.git



